package com.ruoyi.quartz.task;

import java.io.Serializable;
import java.util.GregorianCalendar;


public class DetailsBean implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 用户id
     */
    // @GeneratedValue
    private Integer userId;

    // 国家
    private String country;

    // 地址
    private String address;

    // 省
    private String province;

    // 城市
    private String city;

    // 纬度
    private String latitude;

    // 发表的信息
    private String description;

    // START  上班  END 下班
    private String type;

    // 经度
    private String longitude;

    public DetailsBean() {

        //  结果为“0”是上午 结果为“1”是下午
        GregorianCalendar ca = new GregorianCalendar();
        if (ca.get(GregorianCalendar.AM_PM) == 0 || "0".equals(ca.get(GregorianCalendar.AM_PM))) {
            this.type = "START";
        } else {
            this.type = "STArt";
        }
    }
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


}
