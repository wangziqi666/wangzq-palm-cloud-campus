package com.ruoyi.quartz.task;

import cn.hutool.crypto.SecureUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import com.ruoyi.common.utils.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 定时任务调度测试
 *
 * @author ruoyi
 */
@Component("ryTask")
public class RyTask
{
    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i)
    {
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void ryParams(String params)
    {
        System.out.println("执行有参方法：" + params);
    }

    public void ryTest()
    {
        System.out.println("执行无参方法");
        this.qiandao();
    }
    public void qiandao(){
        Map<String, String> loginmap = login("13304103409", "qq5211314Q");
        String s = planId(loginmap.get("Authorization"), loginmap.get("userId"));
        DetailsBean detailsBean = new DetailsBean();
        detailsBean.setAddress("辽宁省大连市高新园区大连设计城");
        detailsBean.setCity("大连");
        detailsBean.setCountry("中国");
        detailsBean.setDescription("又上班了呢");
        detailsBean.setLatitude("38.849831");
        detailsBean.setProvince("辽宁");
        sin(loginmap.get("Authorization"),s,detailsBean);
    }

    public Map<String,String> login(String username,String password) {

        /* 请求头 */
        Map headersMap = new HashMap();
        headersMap.put("Accept-Language", "zh-CN,zh;q=0.8");
        headersMap.put("User-Agent", "Mozilla/5.0 (Linux; U; Android 8.0.0; zh-cn; MI 6 Build/OPR1.170623.027) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
        headersMap.put("Authorization", "");
        headersMap.put("roleKey", "");
        headersMap.put("Content-Type", "application/json; charset=UTF-8");
        headersMap.put("Content-Length", "85");
        headersMap.put("Host", "api.moguding.net:9000");
        headersMap.put("Connection", "close");
        headersMap.put("Accept-Encoding", "gzip, deflate");
        headersMap.put("Cache-Control", "no-cache");

        /* 请求地址 */
        String url = "https://api.moguding.net:9000/session/user/v1/login";

        /* 请求参数 */
        Map paramsMap = new HashMap();
        paramsMap.put("phone", username);
        paramsMap.put("password", password);
        paramsMap.put("loginType", "android");
        paramsMap.put("uuid", "");

        /* 请求参数转为json字符串 */
        String params = JSON.toJSONString(paramsMap);
        // System.out.println(params);

        try {

            /* 发送请求 */
            System.out.println("url:"+url);
            String post = HttpUtils.post(url, params, headersMap);
            System.out.println(post);

            /* 获取响应参数 */
            String Authorization = JSON.parseObject(post).getString("data");
            Authorization = JSON.parseObject(Authorization).getString("token");
            String UserId=JSON.parseObject(JSON.parseObject(post).getString("data")).getString("userId");
            Map<String,String> map=new HashMap<>();
            map.put("Authorization",Authorization);
            map.put("userId",UserId);
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
    public String planId(String authorization,String userId) {
        /**
         * 算法密钥
         */
       String salt ="3478cbbc33f84bd00d75d7dfa69e0daa";

        /* 请求头 */
        Map headersMap = new HashMap();
        headersMap.put("Accept-Language", "zh-CN,zh;q=0.8");
        headersMap.put("User-Agent", "Mozilla/5.0 (Linux; U; Android 8.0.0; zh-cn; MI 6 Build/OPR1.170623.027) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
        headersMap.put("Authorization", authorization);
        headersMap.put("roleKey", "student");
        headersMap.put("Content-Type", "application/json; charset=UTF-8");
        headersMap.put("Content-Length", "500");
        headersMap.put("Host", "api.moguding.net:9000");
        headersMap.put("Connection", "close");
        headersMap.put("Accept-Encoding", "gzip, deflate");
        headersMap.put("Cache-Control", "no-cache");
        headersMap.put("sign", SecureUtil.md5(userId+"student"+salt));
        /* 请求地址 */
        String url = "https://api.moguding.net:9000/practice/plan/v3/getPlanByStu";
        /* 请求参数 */
        Map paramsMap = new HashMap();
        paramsMap.put("state", "");

        /* 请求参数转为json字符串 */
        String params = JSON.toJSONString(paramsMap);

        try {

            /* 发送请求 */
            String post = HttpUtils.post(url, params, headersMap);
            // System.out.println(post);

            /* 获取响应参数 */
            String planId = JSON.parseObject(post).getString("data");
            JSONArray array = JSON.parseArray(planId);
            planId = array.get(0).toString();
            planId = JSON.parseObject(planId).getString("planId");
             System.out.println(planId);
            return planId;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public void sin(String authorization, String planId, DetailsBean detailsBean) {
        String salt ="3478cbbc33f84bd00d75d7dfa69e0daa";

        /* 请求头 */
        Map headersMap = new HashMap();
        headersMap.put("User-Agent", "Mozilla/5.0 (Linux; U; Android 8.0.0; zh-cn; MI 6 Build/OPR1.170623.027) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
        headersMap.put("Authorization", authorization);
        headersMap.put("Content-Type", "application/json; charset=UTF-8");
        headersMap.put("sign",SecureUtil.md5("Android"+detailsBean.getType()+planId+"102549674"+detailsBean.getAddress()+salt));
        /* 请求地址 */
        String url = "https://api.moguding.net:9000/attendence/clock/v2/save";
        /* 请求参数 */
        Map paramsMap = new HashMap();
        paramsMap.put("country", detailsBean.getCountry());
        paramsMap.put("address", detailsBean.getAddress());
        paramsMap.put("province", detailsBean.getProvince());
        paramsMap.put("city", detailsBean.getCity());
        paramsMap.put("latitude", detailsBean.getLatitude());
        paramsMap.put("description", detailsBean.getDescription());
        paramsMap.put("planId", planId);
        paramsMap.put("type", detailsBean.getType());   // START  上班  END 下班
        paramsMap.put("device", "Android");
        paramsMap.put("state","NORMAL");
        paramsMap.put("longitude", detailsBean.getLongitude());
        /* 请求参数转为json字符串 */
        String params = JSON.toJSONString(paramsMap);

        try {

            /* 发送请求 */
            String post = HttpUtils.post(url, params, headersMap);
            System.out.println(post);

            /* 返回响应参数 */

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("签到失败");
        }
    }

}
