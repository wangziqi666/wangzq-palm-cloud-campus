import request from '@/utils/request'

// 查询寝室楼列表
export function listBuilding(query) {
  return request({
    url: '/system/building/list',
    method: 'get',
    params: query
  })
}

// 查询寝室楼详细
export function getBuilding(id) {
  return request({
    url: '/system/building/' + id,
    method: 'get'
  })
}

// 新增寝室楼
export function addBuilding(data) {
  return request({
    url: '/system/building',
    method: 'post',
    data: data
  })
}

// 修改寝室楼
export function updateBuilding(data) {
  return request({
    url: '/system/building',
    method: 'put',
    data: data
  })
}

// 删除寝室楼
export function delBuilding(id) {
  return request({
    url: '/system/building/' + id,
    method: 'delete'
  })
}
