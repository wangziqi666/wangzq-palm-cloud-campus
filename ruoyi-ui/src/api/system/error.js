import request from '@/utils/request'

// 查询违规管理列表
export function listError(query) {
  return request({
    url: '/system/error/list',
    method: 'get',
    params: query
  })
}

// 查询违规管理详细
export function getError(id) {
  return request({
    url: '/system/error/' + id,
    method: 'get'
  })
}

// 新增违规管理
export function addError(data) {
  return request({
    url: '/system/error',
    method: 'post',
    data: data
  })
}

// 修改违规管理
export function updateError(data) {
  return request({
    url: '/system/error',
    method: 'put',
    data: data
  })
}

// 删除违规管理
export function delError(id) {
  return request({
    url: '/system/error/' + id,
    method: 'delete'
  })
}
