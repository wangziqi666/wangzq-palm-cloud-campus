import request from '@/utils/request'

// 查询大哥远视频列表
export function listVedio(query) {
  return request({
    url: '/system/vedio/list',
    method: 'get',
    params: query
  })
}

// 查询大哥远视频详细
export function getVedio(id) {
  return request({
    url: '/system/vedio/' + id,
    method: 'get'
  })
}

// 新增大哥远视频
export function addVedio(data) {
  return request({
    url: '/system/vedio',
    method: 'post',
    data: data
  })
}

// 修改大哥远视频
export function updateVedio(data) {
  return request({
    url: '/system/vedio',
    method: 'put',
    data: data
  })
}

// 删除大哥远视频
export function delVedio(id) {
  return request({
    url: '/system/vedio/' + id,
    method: 'delete'
  })
}
