package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BuildingMapper;
import com.ruoyi.system.domain.Building;
import com.ruoyi.system.service.IBuildingService;

/**
 * 寝室楼Service业务层处理
 * 
 * @author wangzq
 * @date 2022-01-12
 */
@Service
public class BuildingServiceImpl implements IBuildingService 
{
    @Autowired
    private BuildingMapper buildingMapper;

    /**
     * 查询寝室楼
     * 
     * @param id 寝室楼主键
     * @return 寝室楼
     */
    @Override
    public Building selectBuildingById(Integer id)
    {
        return buildingMapper.selectBuildingById(id);
    }

    /**
     * 查询寝室楼列表
     * 
     * @param building 寝室楼
     * @return 寝室楼
     */
    @Override
    public List<Building> selectBuildingList(Building building)
    {
        return buildingMapper.selectBuildingList(building);
    }

    /**
     * 新增寝室楼
     * 
     * @param building 寝室楼
     * @return 结果
     */
    @Override
    public int insertBuilding(Building building)
    {
        return buildingMapper.insertBuilding(building);
    }

    /**
     * 修改寝室楼
     * 
     * @param building 寝室楼
     * @return 结果
     */
    @Override
    public int updateBuilding(Building building)
    {
        return buildingMapper.updateBuilding(building);
    }

    /**
     * 批量删除寝室楼
     * 
     * @param ids 需要删除的寝室楼主键
     * @return 结果
     */
    @Override
    public int deleteBuildingByIds(Integer[] ids)
    {
        return buildingMapper.deleteBuildingByIds(ids);
    }

    /**
     * 删除寝室楼信息
     * 
     * @param id 寝室楼主键
     * @return 结果
     */
    @Override
    public int deleteBuildingById(Integer id)
    {
        return buildingMapper.deleteBuildingById(id);
    }
}
