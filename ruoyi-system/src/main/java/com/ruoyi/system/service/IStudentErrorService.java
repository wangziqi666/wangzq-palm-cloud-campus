package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.StudentError;

/**
 * 违规管理Service接口
 * 
 * @author ruoyi
 * @date 2022-01-13
 */
public interface IStudentErrorService 
{
    /**
     * 查询违规管理
     * 
     * @param id 违规管理主键
     * @return 违规管理
     */
    public StudentError selectStudentErrorById(Integer id);

    /**
     * 查询违规管理列表
     * 
     * @param studentError 违规管理
     * @return 违规管理集合
     */
    public List<StudentError> selectStudentErrorList(StudentError studentError);

    /**
     * 新增违规管理
     * 
     * @param studentError 违规管理
     * @return 结果
     */
    public int insertStudentError(StudentError studentError);

    /**
     * 修改违规管理
     * 
     * @param studentError 违规管理
     * @return 结果
     */
    public int updateStudentError(StudentError studentError);

    /**
     * 批量删除违规管理
     * 
     * @param ids 需要删除的违规管理主键集合
     * @return 结果
     */
    public int deleteStudentErrorByIds(Integer[] ids);

    /**
     * 删除违规管理信息
     * 
     * @param id 违规管理主键
     * @return 结果
     */
    public int deleteStudentErrorById(Integer id);
}
