package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Building;

/**
 * 寝室楼Service接口
 * 
 * @author wangzq
 * @date 2022-01-12
 */
public interface IBuildingService 
{
    /**
     * 查询寝室楼
     * 
     * @param id 寝室楼主键
     * @return 寝室楼
     */
    public Building selectBuildingById(Integer id);

    /**
     * 查询寝室楼列表
     * 
     * @param building 寝室楼
     * @return 寝室楼集合
     */
    public List<Building> selectBuildingList(Building building);

    /**
     * 新增寝室楼
     * 
     * @param building 寝室楼
     * @return 结果
     */
    public int insertBuilding(Building building);

    /**
     * 修改寝室楼
     * 
     * @param building 寝室楼
     * @return 结果
     */
    public int updateBuilding(Building building);

    /**
     * 批量删除寝室楼
     * 
     * @param ids 需要删除的寝室楼主键集合
     * @return 结果
     */
    public int deleteBuildingByIds(Integer[] ids);

    /**
     * 删除寝室楼信息
     * 
     * @param id 寝室楼主键
     * @return 结果
     */
    public int deleteBuildingById(Integer id);
}
