package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.DageyuanVedio;

/**
 * 大哥远视频Service接口
 * 
 * @author wangzq
 * @date 2022-01-18
 */
public interface IDageyuanVedioService 
{
    /**
     * 查询大哥远视频
     * 
     * @param id 大哥远视频主键
     * @return 大哥远视频
     */
    public DageyuanVedio selectDageyuanVedioById(Long id);

    /**
     * 查询大哥远视频列表
     * 
     * @param dageyuanVedio 大哥远视频
     * @return 大哥远视频集合
     */
    public List<DageyuanVedio> selectDageyuanVedioList(DageyuanVedio dageyuanVedio);

    /**
     * 新增大哥远视频
     * 
     * @param dageyuanVedio 大哥远视频
     * @return 结果
     */
    public int insertDageyuanVedio(DageyuanVedio dageyuanVedio);

    /**
     * 修改大哥远视频
     * 
     * @param dageyuanVedio 大哥远视频
     * @return 结果
     */
    public int updateDageyuanVedio(DageyuanVedio dageyuanVedio);

    /**
     * 批量删除大哥远视频
     * 
     * @param ids 需要删除的大哥远视频主键集合
     * @return 结果
     */
    public int deleteDageyuanVedioByIds(Long[] ids);

    /**
     * 删除大哥远视频信息
     * 
     * @param id 大哥远视频主键
     * @return 结果
     */
    public int deleteDageyuanVedioById(Long id);
}
