package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.StudentErrorMapper;
import com.ruoyi.system.domain.StudentError;
import com.ruoyi.system.service.IStudentErrorService;

/**
 * 违规管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-01-13
 */
@Service
public class StudentErrorServiceImpl implements IStudentErrorService 
{
    @Autowired
    private StudentErrorMapper studentErrorMapper;

    /**
     * 查询违规管理
     * 
     * @param id 违规管理主键
     * @return 违规管理
     */
    @Override
    public StudentError selectStudentErrorById(Integer id)
    {
        return studentErrorMapper.selectStudentErrorById(id);
    }

    /**
     * 查询违规管理列表
     * 
     * @param studentError 违规管理
     * @return 违规管理
     */
    @Override
    public List<StudentError> selectStudentErrorList(StudentError studentError)
    {
        return studentErrorMapper.selectStudentErrorList(studentError);
    }

    /**
     * 新增违规管理
     * 
     * @param studentError 违规管理
     * @return 结果
     */
    @Override
    public int insertStudentError(StudentError studentError)
    {
        return studentErrorMapper.insertStudentError(studentError);
    }

    /**
     * 修改违规管理
     * 
     * @param studentError 违规管理
     * @return 结果
     */
    @Override
    public int updateStudentError(StudentError studentError)
    {
        return studentErrorMapper.updateStudentError(studentError);
    }

    /**
     * 批量删除违规管理
     * 
     * @param ids 需要删除的违规管理主键
     * @return 结果
     */
    @Override
    public int deleteStudentErrorByIds(Integer[] ids)
    {
        return studentErrorMapper.deleteStudentErrorByIds(ids);
    }

    /**
     * 删除违规管理信息
     * 
     * @param id 违规管理主键
     * @return 结果
     */
    @Override
    public int deleteStudentErrorById(Integer id)
    {
        return studentErrorMapper.deleteStudentErrorById(id);
    }
}
