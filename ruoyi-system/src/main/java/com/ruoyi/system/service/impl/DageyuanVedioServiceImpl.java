package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DageyuanVedioMapper;
import com.ruoyi.system.domain.DageyuanVedio;
import com.ruoyi.system.service.IDageyuanVedioService;

/**
 * 大哥远视频Service业务层处理
 * 
 * @author wangzq
 * @date 2022-01-18
 */
@Service
public class DageyuanVedioServiceImpl implements IDageyuanVedioService 
{
    @Autowired
    private DageyuanVedioMapper dageyuanVedioMapper;

    /**
     * 查询大哥远视频
     * 
     * @param id 大哥远视频主键
     * @return 大哥远视频
     */
    @Override
    public DageyuanVedio selectDageyuanVedioById(Long id)
    {
        return dageyuanVedioMapper.selectDageyuanVedioById(id);
    }

    /**
     * 查询大哥远视频列表
     * 
     * @param dageyuanVedio 大哥远视频
     * @return 大哥远视频
     */
    @Override
    public List<DageyuanVedio> selectDageyuanVedioList(DageyuanVedio dageyuanVedio)
    {
        return dageyuanVedioMapper.selectDageyuanVedioList(dageyuanVedio);
    }

    /**
     * 新增大哥远视频
     * 
     * @param dageyuanVedio 大哥远视频
     * @return 结果
     */
    @Override
    public int insertDageyuanVedio(DageyuanVedio dageyuanVedio)
    {
        return dageyuanVedioMapper.insertDageyuanVedio(dageyuanVedio);
    }

    /**
     * 修改大哥远视频
     * 
     * @param dageyuanVedio 大哥远视频
     * @return 结果
     */
    @Override
    public int updateDageyuanVedio(DageyuanVedio dageyuanVedio)
    {
        return dageyuanVedioMapper.updateDageyuanVedio(dageyuanVedio);
    }

    /**
     * 批量删除大哥远视频
     * 
     * @param ids 需要删除的大哥远视频主键
     * @return 结果
     */
    @Override
    public int deleteDageyuanVedioByIds(Long[] ids)
    {
        return dageyuanVedioMapper.deleteDageyuanVedioByIds(ids);
    }

    /**
     * 删除大哥远视频信息
     * 
     * @param id 大哥远视频主键
     * @return 结果
     */
    @Override
    public int deleteDageyuanVedioById(Long id)
    {
        return dageyuanVedioMapper.deleteDageyuanVedioById(id);
    }
}
