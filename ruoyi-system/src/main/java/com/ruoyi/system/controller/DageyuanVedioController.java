package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.DageyuanVedio;
import com.ruoyi.system.service.IDageyuanVedioService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 大哥远视频Controller
 *
 * @author wangzq
 * @date 2022-01-18
 */
@RestController
@RequestMapping("/system/vedio")
public class DageyuanVedioController extends BaseController
{
    @Autowired
    private IDageyuanVedioService dageyuanVedioService;

    /**
     * 查询大哥远视频列表
     */
//    @PreAuthorize("@ss.hasPermi('system:vedio:list')")
    @GetMapping("/list")
    public TableDataInfo list(DageyuanVedio dageyuanVedio)
    {
        startPage();
        List<DageyuanVedio> list = dageyuanVedioService.selectDageyuanVedioList(dageyuanVedio);
        return getDataTable(list);
    }

    /**
     * 导出大哥远视频列表
     */
    @PreAuthorize("@ss.hasPermi('system:vedio:export')")
    @Log(title = "大哥远视频", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DageyuanVedio dageyuanVedio)
    {
        List<DageyuanVedio> list = dageyuanVedioService.selectDageyuanVedioList(dageyuanVedio);
        ExcelUtil<DageyuanVedio> util = new ExcelUtil<DageyuanVedio>(DageyuanVedio.class);
        util.exportExcel(response, list, "大哥远视频数据");
    }

    /**
     * 获取大哥远视频详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:vedio:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(dageyuanVedioService.selectDageyuanVedioById(id));
    }

    /**
     * 新增大哥远视频
     */
    @PreAuthorize("@ss.hasPermi('system:vedio:add')")
    @Log(title = "大哥远视频", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DageyuanVedio dageyuanVedio)
    {
        return toAjax(dageyuanVedioService.insertDageyuanVedio(dageyuanVedio));
    }

    /**
     * 修改大哥远视频
     */
    @PreAuthorize("@ss.hasPermi('system:vedio:edit')")
    @Log(title = "大哥远视频", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DageyuanVedio dageyuanVedio)
    {
        return toAjax(dageyuanVedioService.updateDageyuanVedio(dageyuanVedio));
    }

    /**
     * 删除大哥远视频
     */
    @PreAuthorize("@ss.hasPermi('system:vedio:remove')")
    @Log(title = "大哥远视频", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dageyuanVedioService.deleteDageyuanVedioByIds(ids));
    }
}
