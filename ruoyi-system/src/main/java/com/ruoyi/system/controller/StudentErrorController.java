package com.ruoyi.system.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.Building;
import com.ruoyi.system.domain.vo.StudentErrorVo;
import com.ruoyi.system.service.IBuildingService;
import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.StudentError;
import com.ruoyi.system.service.IStudentErrorService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 违规管理Controller
 * 
 * @author ruoyi
 * @date 2022-01-13
 */
@RestController
@RequestMapping("/system/error")
public class StudentErrorController extends BaseController
{
    @Autowired
    private IStudentErrorService studentErrorService;

    @Autowired
    private IBuildingService buildingService;


    /**
     * 查询违规管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:error:list')")
    @GetMapping("/list")
    public TableDataInfo list(StudentError studentError)
    {
        startPage();
        List<StudentError> list = studentErrorService.selectStudentErrorList(studentError);
        List<StudentErrorVo> list1=new ArrayList<>();
        for (StudentError error : list) {
            StudentErrorVo studentErrorVo=new StudentErrorVo();
            BeanUtils.copyProperties(error,studentErrorVo);
            Building building = buildingService.selectBuildingById(error.getBuildingId());
            studentErrorVo.setBuilding(building);
            list1.add(studentErrorVo);
        }
        return getDataTable(list1);
    }

    /**
     * 导出违规管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:error:export')")
    @Log(title = "违规管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StudentError studentError)
    {
        List<StudentError> list = studentErrorService.selectStudentErrorList(studentError);
        ExcelUtil<StudentError> util = new ExcelUtil<StudentError>(StudentError.class);
        util.exportExcel(response, list, "违规管理数据");
    }

    /**
     * 获取违规管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:error:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        StudentError studentError = studentErrorService.selectStudentErrorById(id);
        Building building = buildingService.selectBuildingById(studentError.getBuildingId());
        StudentErrorVo studentErrorVo=new StudentErrorVo();
        BeanUtils.copyProperties(studentError,studentErrorVo);
        studentErrorVo.setBuilding(building);
        return AjaxResult.success(studentErrorVo);
    }

    /**
     * 新增违规管理
     */
    @PreAuthorize("@ss.hasPermi('system:error:add')")
    @Log(title = "违规管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StudentError studentError)
    {
        return toAjax(studentErrorService.insertStudentError(studentError));
    }

    /**
     * 修改违规管理
     */
    @PreAuthorize("@ss.hasPermi('system:error:edit')")
    @Log(title = "违规管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StudentErrorVo studentError)
    {
        return toAjax(studentErrorService.updateStudentError(studentError));
    }

    /**
     * 删除违规管理
     */
    @PreAuthorize("@ss.hasPermi('system:error:remove')")
    @Log(title = "违规管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(studentErrorService.deleteStudentErrorByIds(ids));
    }
}
