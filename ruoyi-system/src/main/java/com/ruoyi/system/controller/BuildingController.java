package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Building;
import com.ruoyi.system.service.IBuildingService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 寝室楼Controller
 * 
 * @author wangzq
 * @date 2022-01-12
 */
@RestController
@RequestMapping("/system/building")
public class BuildingController extends BaseController
{
    @Autowired
    private IBuildingService buildingService;

    /**
     * 查询寝室楼列表
     */
    @PreAuthorize("@ss.hasPermi('system:building:list')")
    @GetMapping("/list")
    public TableDataInfo list(Building building)
    {
        startPage();
        List<Building> list = buildingService.selectBuildingList(building);
        return getDataTable(list);
    }

    /**
     * 导出寝室楼列表
     */
    @PreAuthorize("@ss.hasPermi('system:building:export')")
    @Log(title = "寝室楼", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Building building)
    {
        List<Building> list = buildingService.selectBuildingList(building);
        ExcelUtil<Building> util = new ExcelUtil<Building>(Building.class);
        util.exportExcel(response, list, "寝室楼数据");
    }

    /**
     * 获取寝室楼详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:building:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(buildingService.selectBuildingById(id));
    }

    /**
     * 新增寝室楼
     */
    @PreAuthorize("@ss.hasPermi('system:building:add')")
    @Log(title = "寝室楼", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Building building)
    {
        return toAjax(buildingService.insertBuilding(building));
    }

    /**
     * 修改寝室楼
     */
    @PreAuthorize("@ss.hasPermi('system:building:edit')")
    @Log(title = "寝室楼", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Building building)
    {
        return toAjax(buildingService.updateBuilding(building));
    }

    /**
     * 删除寝室楼
     */
    @PreAuthorize("@ss.hasPermi('system:building:remove')")
    @Log(title = "寝室楼", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(buildingService.deleteBuildingByIds(ids));
    }

}
