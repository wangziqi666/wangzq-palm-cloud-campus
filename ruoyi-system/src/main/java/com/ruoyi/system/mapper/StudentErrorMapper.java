package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.StudentError;

/**
 * 违规管理Mapper接口
 * 
 * @author ruoyi
 * @date 2022-01-13
 */
public interface StudentErrorMapper 
{
    /**
     * 查询违规管理
     * 
     * @param id 违规管理主键
     * @return 违规管理
     */
    public StudentError selectStudentErrorById(Integer id);

    /**
     * 查询违规管理列表
     * 
     * @param studentError 违规管理
     * @return 违规管理集合
     */
    public List<StudentError> selectStudentErrorList(StudentError studentError);

    /**
     * 新增违规管理
     * 
     * @param studentError 违规管理
     * @return 结果
     */
    public int insertStudentError(StudentError studentError);

    /**
     * 修改违规管理
     * 
     * @param studentError 违规管理
     * @return 结果
     */
    public int updateStudentError(StudentError studentError);

    /**
     * 删除违规管理
     * 
     * @param id 违规管理主键
     * @return 结果
     */
    public int deleteStudentErrorById(Integer id);

    /**
     * 批量删除违规管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStudentErrorByIds(Integer[] ids);
}
