package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 寝室楼对象 building
 * 
 * @author wangzq
 * @date 2022-01-12
 */
public class Building extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Integer id;

    /** 寝室楼号 */
    @Excel(name = "寝室楼号")
    private Integer bulidingId;

    /** 寝室楼名 */
    @Excel(name = "寝室楼名")
    private String buildingName;

    /** 性别 */
    @Excel(name = "性别")
    private String buildingSex;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setBulidingId(Integer bulidingId) 
    {
        this.bulidingId = bulidingId;
    }

    public Integer getBulidingId() 
    {
        return bulidingId;
    }
    public void setBuildingName(String buildingName) 
    {
        this.buildingName = buildingName;
    }

    public String getBuildingName() 
    {
        return buildingName;
    }
    public void setBuildingSex(String buildingSex) 
    {
        this.buildingSex = buildingSex;
    }

    public String getBuildingSex() 
    {
        return buildingSex;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("bulidingId", getBulidingId())
            .append("buildingName", getBuildingName())
            .append("buildingSex", getBuildingSex())
            .toString();
    }
}
