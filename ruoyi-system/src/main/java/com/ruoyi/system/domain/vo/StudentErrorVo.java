package com.ruoyi.system.domain.vo;

import com.ruoyi.system.domain.Building;
import com.ruoyi.system.domain.StudentError;
public class StudentErrorVo extends StudentError {
    public Building building;

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }
}
