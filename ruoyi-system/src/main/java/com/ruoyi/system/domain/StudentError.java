package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 违规管理对象 student_error
 * 
 * @author ruoyi
 * @date 2022-01-13
 */
public class StudentError extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Integer id;

    /** 学生姓名 */
    @Excel(name = "学生姓名")
    private String studentName;

    /** 扣除学分 */
    @Excel(name = "扣除学分")
    private Long editScore;

    /** 所在楼 */
    @Excel(name = "所在楼")
    private Integer buildingId;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setStudentName(String studentName) 
    {
        this.studentName = studentName;
    }

    public String getStudentName() 
    {
        return studentName;
    }
    public void setEditScore(Long editScore) 
    {
        this.editScore = editScore;
    }

    public Long getEditScore() 
    {
        return editScore;
    }
    public void setBuildingId(Integer buildingId) 
    {
        this.buildingId = buildingId;
    }

    public Integer getBuildingId() 
    {
        return buildingId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("studentName", getStudentName())
            .append("editScore", getEditScore())
            .append("buildingId", getBuildingId())
            .toString();
    }
}
